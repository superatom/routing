<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RouterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Superatom\Routing\Router
     */
    protected $router;

    protected function setUp()
    {
        $this->router = new \Superatom\Routing\Router();
    }

    protected function tearDown()
    {
        $this->router = null;
    }

    public function testRoute()
    {
        $this->router->get('/', function () { return 'test'; });

        $res = $this->dispatch('GET', '/');

        $this->assertSameBody('test', $res);
    }

    public function testDefaultRouteResolver()
    {
        $this->router->get('/profile/{user}', function ($req, $res, $args) { return 'test '.$args['user']; });

        $res = $this->dispatch('GET', '/profile/foo');

        $this->assertSameBody('test foo', $res);
    }

    public function testDefaultNotAllowedResolver()
    {
        $this->router->get('/', function () { return 'test'; });
        $this->router->post('/', function () { return 'test'; });

        $res = $this->dispatch('DELETE', '/');

        $this->assertSame(405, $res->getStatusCode());
        $this->assertSame('GET, POST', $res->headers->get('Allow'));
    }

    public function testDefaultNotFoundResolver()
    {
        $res = $this->dispatch('GET', '/');

        $this->assertTrue($res->isNotFound());
    }

    public function testGroupRoute()
    {
        $this->router->group('/group', function () {
            $this->router->group('/group2', function () {
                $this->router->get('/test', function () { return 'test group'; });
            });
        });

        $res = $this->dispatch('GET', '/group/group2/test');

        $this->assertSameBody('test group', $res);
    }

    public function testRouteMiddleware()
    {
        $this->router->get('/', function () { return 'test'; })->add($this->createMiddleware());

        $res = $this->dispatch('GET', '/');

        $this->assertSameBody('BEFORE test AFTER', $res);
    }

    public function testGroupRouteMiddleware()
    {
        $this->router->group('/group', function () {
            $this->router->group('/group2', function () {
                $this->router->get('/test', function () { return 'test group'; });
            })->add($this->createMiddleware('BEFORE_G2 ', ' AFTER_G2'));
        })->add($this->createMiddleware('BEFORE_G1 ', ' AFTER_G1'));

        $res = $this->dispatch('GET', '/group/group2/test');

        $this->assertSameBody('BEFORE_G1 BEFORE_G2 test group AFTER_G2 AFTER_G1', $res);
    }

    public function testAllMiddleware()
    {
        $this->router->group('/group', function () {
            $this->router->get('/route1', function () { return 'route1'; });
            // add middleware for route2 only
            $this->router->get('/route2', function () { return 'route2'; })
                ->add($this->createMiddleware());
            // add middleware for both route
        })->add($this->createMiddleware('GROUP_BEFORE ', ' GROUP_AFTER'));

        $this->router->get('/route3', function () { return 'route3'; });

        // add middleware for all route
        $this->router->add($this->createMiddleware('ALL_BEFORE ', ' ALL_AFTER'));

        $res1 = $this->dispatch('GET', '/group/route1');
        $res2 = $this->dispatch('GET', '/group/route2');
        $res3 = $this->dispatch('GET', '/route3');

        $this->assertSameBody('ALL_BEFORE GROUP_BEFORE route1 GROUP_AFTER ALL_AFTER', $res1);
        $this->assertSameBody('ALL_BEFORE GROUP_BEFORE BEFORE route2 AFTER GROUP_AFTER ALL_AFTER', $res2);
        $this->assertSameBody('ALL_BEFORE route3 ALL_AFTER', $res3);
    }

    public function testReverseRouting()
    {
        $this->router->get('/test/{arg1}/{arg2}', 'handler')->setName('test');

        $path = $this->router->pathFor('test', ['arg1' => 'foo', 'arg2' => 'bar']);

        $this->assertSame('/test/foo/bar', $path);
    }

    public function testParserShortcut()
    {
        $this->router->get('/foo/{bar:i}', function () { return 'baz'; });

        $res1 = $this->dispatch('GET', '/foo/abc');
        $res2 = $this->dispatch('GET', '/foo/2');

        $this->assertTrue($res1->isNotFound());
        $this->assertTrue($res2->isOk());
    }

    public function testDisableMiddleware()
    {
        $this->router->add($this->createMiddleware());

        $this->router->get('/disable', function () { return 'disable middleware'; })
            ->add($this->createMiddleware());

        $this->router->disableMiddleware();

        $res = $this->dispatch('GET', '/disable');

        $this->assertSameBody('disable middleware', $res);
    }

    private function dispatch($method, $uri)
    {
        $req = Request::create($uri, $method);
        $res = new Response();

        return $this->router->dispatch($req, $res);
    }

    /**
     * @param string $before
     * @param string $after
     *
     * @return Closure
     */
    private function createMiddleware($before = 'BEFORE ', $after = ' AFTER')
    {
        return function (Request $req, Response $res, callable $next) use ($before, $after) {
            $res->setContent($res->getContent().$before);
            $res = $next($req, $res);
            /* @var Response $res */
            $res->setContent($res->getContent().$after);

            return $res;
        };
    }

    private function assertSameBody($expectedBody, Response $res)
    {
        $this->assertTrue($res->isOk());
        $this->assertSame($expectedBody, $res->getContent());
    }
}
