<?php

namespace Superatom\Routing;

class RouteGroup
{
    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var callable[]
     */
    protected $middleware = [];

    public function __construct($pattern)
    {
        $this->pattern = $pattern;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function getMiddleware()
    {
        return $this->middleware;
    }

    public function add($middleware)
    {
        $this->middleware[] = $middleware;

        return $this;
    }
}
