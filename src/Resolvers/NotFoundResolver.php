<?php

namespace Superatom\Routing\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotFoundResolver
{
    /**
     * Invoke not found handler.
     *
     * @param Request  $req The most recent Request object
     * @param Response $res The most recent Response object
     *
     * @return Response
     */
    public function __invoke(Request $req, Response $res)
    {
        return new Response('404 not found', 404);
    }
}
