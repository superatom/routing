<?php

namespace Superatom\Routing\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotAllowedResolver
{
    /**
     * Invoke not allowed handler.
     *
     * @param Request  $req     The most recent Request object
     * @param Response $res     The most recent Response object
     * @param array    $methods
     *
     * @return Response
     */
    public function __invoke(Request $req, Response $res, array $methods)
    {
        $methods = implode(', ', $methods);

        return new Response('Allow methods: '.$methods, 405, ['Allow' => $methods]);
    }
}
