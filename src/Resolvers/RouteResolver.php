<?php

namespace Superatom\Routing\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RouteResolver
{
    /**
     * @param callable $callable
     * @param Request  $req
     * @param Response $res
     *
     * @return Response
     */
    public function __invoke(callable $callable, Request $req, Response $res)
    {
        return $callable($req, $res, $req->attributes->all());
    }
}
