<?php

namespace Superatom\Routing;

use FastRoute\RouteParser\Std as StdParser;

class RouteParser extends StdParser
{
    /**
     * route variable constraints shortcut.
     *
     * @var array
     */
    protected $regexShortcuts = array(
        ':i}' => ':[0-9]+}',
        ':a}' => ':[0-9A-Za-z]+}',
        ':h}' => ':[0-9A-Fa-f]+}',
        ':c}' => ':[a-zA-Z0-9+_\-\.]+}',
    );

    public function parse($route)
    {
        $route = strtr($route, $this->regexShortcuts);

        return parent::parse($route);
    }
}
