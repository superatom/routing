<?php

namespace Superatom\Routing;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Route
{
    use MiddlewareAwareTrait;

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var callable
     */
    protected $handler;

    /**
     * @var RouteGroup[]
     */
    protected $groups;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var callable[]
     */
    protected $routeMiddleware = [];

    /**
     * @var callable
     */
    protected $routeResolver;

    public function __construct($methods, $pattern, $handler, array $groups = [])
    {
        $this->methods = $methods;
        $this->pattern = $pattern;
        $this->handler = $handler;
        $this->groups = $groups;
    }

    public function getMethods()
    {
        return $this->methods;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @return RouteGroup[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setRouteResolver(callable $resolver)
    {
        $this->routeResolver = $resolver;

        return $this;
    }

    public function run(Request $req, Response $res, $useMiddleware = true)
    {
        foreach ($this->getGroups() as $group) {
            foreach ($group->getMiddleware() as $mw) {
                $this->middleware[] = $mw;
            }
        }

        return $this->callStackMiddleware($req, $res, $useMiddleware);
    }

    public function __invoke(Request $req, Response $res)
    {
        /** @var Response $newResponse */
        $newResponse = call_user_func_array($this->routeResolver, [$this->handler, $req, $res]);

        if ($newResponse instanceof Response) {
            // if route callback returns a Response, then use it
            $res = $newResponse;
        } elseif (is_string($newResponse)) {
            // if route callback returns a string, then append it to the response
            $res->setContent($res->getContent().$newResponse);
        }

        return $res->prepare($req);
    }
}
