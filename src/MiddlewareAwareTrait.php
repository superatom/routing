<?php

namespace Superatom\Routing;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait MiddlewareAwareTrait
{
    /**
     * @var callable[]
     */
    protected $middleware = [];

    public function add(callable $middleware)
    {
        $this->middleware[] = $middleware;

        return $this;
    }

    /**
     * @param Request  $req
     * @param Response $res
     * @param bool     $useMiddleware
     * @param callable $kernel
     *
     * @return Response
     */
    public function callStackMiddleware(Request $req, Response $res, $useMiddleware = true, $kernel = null)
    {
        if (is_null($kernel)) {
            $kernel = $this;
        }

        $stack = [$kernel];

        if ($useMiddleware) {
            foreach ($this->middleware as $mw) {
                array_unshift($stack, $this->createHandler($mw, $stack[0]));
            }
        }

        return $stack[0]($req, $res);
    }

    /**
     * @param callable $middleware
     * @param callable $next
     *
     * @return \Closure
     */
    protected function createHandler(callable $middleware, callable $next)
    {
        return function (Request $req, Response $res) use ($middleware, $next) {
            $newResponse = call_user_func($middleware, $req, $res, $next);
            if ($newResponse instanceof Response === false) {
                throw new \UnexpectedValueException('Middleware must return instance of \Symfony\Component\HttpFoundation\Response');
            }

            return $newResponse;
        };
    }
}
