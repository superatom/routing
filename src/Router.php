<?php

namespace Superatom\Routing;

use FastRoute\DataGenerator as DataGeneratorInterface;
use FastRoute\DataGenerator\GroupCountBased as GroupCountBasedGenerator;
use FastRoute\Dispatcher\GroupCountBased as GroupCountBasedDispatcher;
use FastRoute\RouteCollector;
use FastRoute\RouteParser as RouteParserInterface;
use Superatom\Routing\Resolvers\RouteResolver;
use Superatom\Routing\Resolvers\NotAllowedResolver;
use Superatom\Routing\Resolvers\NotFoundResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Router extends RouteCollector
{
    use MiddlewareAwareTrait;

    /**
     * @var RouteParser
     */
    protected $parser;

    /**
     * @var Route[]
     */
    protected $routes = [];

    /**
     * @var Route[]
     */
    protected $namedRoutes = [];

    /**
     * @var RouteGroup[]
     */
    protected $groups = [];

    /**
     * @var Route
     */
    protected $currentRoute;

    /**
     * @var callable
     */
    protected $routeResolver;

    /**
     * @var callable
     */
    protected $notAllowedResolver;

    /**
     * @var callable
     */
    protected $notFoundResolver;

    /**
     * @var bool
     */
    protected $useMiddleware = true;

    public function __construct(RouteParserInterface $parser = null, DataGeneratorInterface $generator = null)
    {
        $parser = $parser ?: new RouteParser();
        $generator = $generator ?: new GroupCountBasedGenerator();
        parent::__construct($parser, $generator);

        $this->parser = $parser;
        $this->routeResolver = new RouteResolver();
        $this->notAllowedResolver = new NotAllowedResolver();
        $this->notFoundResolver = new NotFoundResolver();
    }

    public function setRouteResolver(callable $handler)
    {
        $this->routeResolver = $handler;

        return $this;
    }

    public function setNotAllowedResolver(callable $handler)
    {
        $this->notAllowedResolver = $handler;

        return $this;
    }

    public function setNotFoundResolver(callable $handler)
    {
        $this->notFoundResolver = $handler;

        return $this;
    }

    public function enableMiddleware()
    {
        $this->useMiddleware = true;

        return $this;
    }

    public function disableMiddleware()
    {
        $this->useMiddleware = false;

        return $this;
    }

    public function map($methods, $pattern, $handler)
    {
        if ($this->groups) {
            $pattern = $this->processGroups().$pattern;
        }

        $route = new Route($methods, $pattern, $handler, array_reverse($this->groups));
        $this->addRoute($route->getMethods(), $route->getPattern(), [$route, 'run']);

        return $this->routes[] = $route;
    }

    public function get($pattern, $handler)
    {
        return $this->map(['GET'], $pattern, $handler);
    }

    public function post($pattern, $handler)
    {
        return $this->map(['POST'], $pattern, $handler);
    }

    public function put($pattern, $handler)
    {
        return $this->map(['PUT'], $pattern, $handler);
    }

    public function patch($pattern, $handler)
    {
        return $this->map(['PATCH'], $pattern, $handler);
    }

    public function delete($pattern, $handler)
    {
        return $this->map(['DELETE'], $pattern, $handler);
    }

    public function options($pattern, $handler)
    {
        return $this->map(['OPTIONS'], $pattern, $handler);
    }

    public function any($pattern, $handler)
    {
        return $this->map(['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'], $pattern, $handler);
    }

    public function group($pattern, $callable)
    {
        $group = new RouteGroup($pattern, $callable);

        array_push($this->groups, $group);
        call_user_func($callable);
        array_pop($this->groups);

        return $group;
    }

    /**
     * @param Request  $req
     * @param Response $res
     *
     * @return Response
     */
    public function dispatch(Request $req, Response $res)
    {
        return $this->callStackMiddleware($req, $res, $this->useMiddleware, [$this, 'handle']);
    }

    /**
     * @param Request  $req
     * @param Response $res
     *
     * @return Response
     */
    public function handle(Request $req, Response $res)
    {
        $dispatcher = new GroupCountBasedDispatcher($this->getData());

        $routeInfo = $dispatcher->dispatch(
            $req->getMethod(),
            $req->getPathInfo()
        );

        if ($routeInfo[0] === GroupCountBasedDispatcher::FOUND) {
            /*
             * Found routeInfo structure
             *
             * $routeInfo = array(
             *      0 => 1, // means Dispatcher route found
             *      1 => array( // callable
             *          0 => $route // Route class instance
             *          1 => 'run' // only callable method name
             *      ),
             *      2 => array('key' => 'val') // route variables
             * )
             */
            $this->currentRoute = $routeInfo[1][0];
            $this->currentRoute->setRouteResolver($this->routeResolver);
            foreach ($routeInfo[2] as $k => $v) {
                $req->attributes->set($k, urldecode($v));
            }

            // call Route instance run method
            return $routeInfo[1]($req, $res, $this->useMiddleware);
        } elseif ($routeInfo[0] === GroupCountBasedDispatcher::METHOD_NOT_ALLOWED) {
            $allowedMethods = $routeInfo[1];

            return call_user_func_array($this->notAllowedResolver, [$req, $res, $allowedMethods]);
        }

        return call_user_func_array($this->notFoundResolver, [$req, $res]);
    }

    /**
     * Get current dispatch route.
     *
     * @return Route
     */
    public function getCurrentRoute()
    {
        return $this->currentRoute;
    }

    /**
     * Get route objects.
     *
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Build the path for a named route.
     *
     * @param string $name        Route name
     * @param array  $data        Named argument replacement data
     * @param array  $queryParams Optional query string parameters
     *
     * @return string
     *
     * @throws \RuntimeException         If named route does not exist
     * @throws \InvalidArgumentException If required data not provided
     */
    public function pathFor($name, array $data = [], array $queryParams = [])
    {
        if (empty($this->namedRoutes)) {
            $this->buildNameIndex();
        }
        if (!isset($this->namedRoutes[$name])) {
            throw new \RuntimeException('Named route does not exist for name: '.$name);
        }
        $route = $this->namedRoutes[$name];
        $pattern = $route->getPattern();
        $routes = $this->parser->parse($pattern);
        // $routes is an array of all possible routes that can be made. There is
        // one route for each optional parameter plus one for no optional parameters.
        //
        // The most specific is last, so we look for that first.
        $routes = array_reverse($routes);
        $segments = [];
        foreach ($routes as $routeData) {
            foreach ($routeData as $item) {
                if (is_string($item)) {
                    // this segment is a static string
                    $segments[] = $item;
                    continue;
                }
                // This segment has a parameter: first element is the name
                if (!array_key_exists($item[0], $data)) {
                    // we don't have a data element for this segment: cancel
                    // testing this route item, so that we can try a less
                    // specific route item.
                    $segments = [];
                    $segmentName = $item[0];
                    break;
                }
                $segments[] = $data[$item[0]];
            }
            if (!empty($segments)) {
                // we found all the parameters for this route data, no need to check
                // less specific ones
                break;
            }
        }

        if (empty($segments) && isset($segmentName)) {
            throw new \InvalidArgumentException('Missing data for URL segment: '.$segmentName);
        }
        $url = implode('', $segments);
        if ($queryParams) {
            $url .= '?'.http_build_query($queryParams);
        }

        return $url;
    }

    protected function processGroups()
    {
        $pattern = '';
        foreach ($this->groups as $group) {
            $pattern .= $group->getPattern();
        }

        return $pattern;
    }

    /**
     * Build index of named routes.
     */
    protected function buildNameIndex()
    {
        $this->namedRoutes = [];
        foreach ($this->routes as $route) {
            $name = $route->getName();
            if ($name) {
                $this->namedRoutes[$name] = $route;
            }
        }
    }
}
